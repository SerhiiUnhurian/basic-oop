#include <iostream>
#include "Gun.h"

Gun::Gun(const std::string& model, int capacity) {
	amount = 0;
	isReady = false;
	totalShots = 0;
	this->model = model;
	this->capacity = capacity;
}
Gun::~Gun() {}

int Gun::getAmount() const {
	return amount;
}
int Gun::getCapacity() const {
	return capacity;
}
bool Gun::ready() const {
	return isReady;
}
const std::string& Gun::getModel() const {
	return model;
}
int Gun::getTotalShots() const {
	return totalShots;
}

void Gun::prepare() {
	isReady = !isReady;
}
void Gun::reload() {
	amount = capacity;
}
void Gun::shoot() {
	if ( !ready() ) {
		throw NotReady();
	}
	if ( amount == 0 ) {
		throw OutOfRounds();
	}

	std::cout << "Bang!" << std::endl;
	amount -= 1;
    totalShots += 1;
	isReady = !isReady;
}

std::ostream& operator<<(std::ostream& out, const Gun& gun) {
	out << "Amount = " << gun.getAmount() << std::endl;
	out << "Capacity = " << gun.getCapacity() << std::endl; 
	out << "isReady = " << gun.ready() << std::endl; 
	out << "Model = " << gun.getModel() << std::endl;
	out << "Total shots = " << gun.getTotalShots() << std::endl;
	return out;
}