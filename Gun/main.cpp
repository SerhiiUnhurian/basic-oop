#include <iostream>
#include "Gun.h"

int main() {
	try {
		Gun gun;
		Gun beretta("Beretta", 10);

		std::cout << gun << std::endl;
		std::cout << beretta << std::endl;

		beretta.reload();
		beretta.prepare();

		beretta.shoot();

		beretta.prepare();
		beretta.shoot();
		
		std::cout << gun << std::endl;
		std::cout << beretta << std::endl;
	}
	catch ( const NotReady excep ) {
		std::cout << "Gun is not ready. Prepare the gun!" << std::endl;  
	}
	catch ( const OutOfRounds excep) {
		std::cout << "Gun is out of rounds. Reload the gun!" << std::endl;  
	}

	return 0;
}
