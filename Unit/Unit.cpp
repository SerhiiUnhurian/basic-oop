#include <iostream>
#include <string>
#include "Unit.h"

Unit::Unit(const std::string& name, int hp, int dmg) {
    this->name = name;
    hitPointsLimit = hp;
    hitPoints = hp;
    damage = dmg;
}

Unit::~Unit() {}

void Unit::ensureIsAlive() {
    if ( hitPoints == 0 ) {
        throw UnitIsDead();
    }
}

int Unit::getDamage() const {
    return damage;
}
int Unit::getHitPoints() const {
    return hitPoints;
}
int Unit::getHitPointsLimit() const {
    return hitPointsLimit;
}
const std::string& Unit::getName() const {
    return name;
}

void Unit::addHitPoints(int hp) {
    int hpLack = hitPointsLimit - hitPoints;
    // try {
        ensureIsAlive();
    // }
    // catch(const UnitIsDead error) {
    //     std::cout << "Unit is dead." << endl;
    // }

    if ( hp < hpLack ) {
        hitPoints += hp;
    } else {
        hitPoints = hitPointsLimit;
    }
}
void Unit::takeDamage(int dmg) {
    ensureIsAlive();
    if ( dmg < hitPoints ) {
        hitPoints -= dmg;
    } else {
        hitPoints = 0;
    }
}

void Unit::attack(Unit& enemy) {
    enemy.takeDamage(damage);
    enemy.ensureIsAlive();
    enemy.counterAttack(*this);
}
void Unit::counterAttack(Unit& enemy){
    enemy.takeDamage(damage/2);
}

std::ostream& operator<<(std::ostream& out, const Unit& unit) {
    out << "Name = " << unit.getName() << std::endl;
    out << "Hit Points Limit = " << unit.getHitPointsLimit() << std::endl; 
    out << "Hit Points = " << unit.getHitPoints() << std::endl; 
    out << "Damage = " << unit.getDamage() << std::endl; 

    return out; 
}
