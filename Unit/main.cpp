#include <iostream>
#include "Unit.h"

int main() {
    Unit barbarian("Barbarian", 200, 170);
    Unit knight("Knight", 180, 200);

    std::cout << barbarian << std::endl;
    std::cout << knight << std::endl;

    try {
        barbarian.attack(knight);
        std::cout << barbarian << std::endl;
        std::cout << knight << std::endl;

        knight.addHitPoints(100);

    }
    catch(const UnitIsDead error) {
        std::cout << "Unit is dead." << std::endl;
    }

    std::cout << barbarian << std::endl;
    std::cout << knight << std::endl;

    return 0;
}