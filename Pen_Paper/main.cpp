#include <iostream>
#include "Paper.h"
#include "Pen.h"

int main() {
	Paper paper(10);
	Pen pen(5);

	try {
		pen.write(paper, "Sergey Unguryan");
		paper.show();
		std::cout << "Ink amount = "<< pen.getInkAmount() << std::endl; 
		std::cout << "Symbols written = "<< paper.getSymbols() << std::endl; 
		std::cout << "Allowed Symbols = "<< paper.getMaxSymbols() - paper.getSymbols() << std::endl; 

		pen.refill();
		std::cout << "Ink amount = "<< pen.getInkAmount() << std::endl; 
		std::cout << "Symbols written = "<< paper.getSymbols() << std::endl;

		pen.write(paper, "23333333333");
		paper.show();
	    std::cout << "Ink amount = "<< pen.getInkAmount() << std::endl; 
		std::cout << "Symbols written = "<< paper.getSymbols() << std::endl;
		std::cout << "Allowed Symbols = "<< paper.getMaxSymbols() - paper.getSymbols() << std::endl; 
		
	}
	catch(const OutOfInk error) {
        std::cout << "Out of ink! Refill the pen." << std::endl;
    } 
    catch(const OutOfSpace error) {
        std::cout << "Out of space on paper." << std::endl; 
    } 

    return 0;
}