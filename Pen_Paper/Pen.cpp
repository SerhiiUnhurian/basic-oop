#include <iostream>
#include "Paper.h"
#include "Pen.h"

Pen::Pen(int inkCapacity) {
    this->inkCapacity = inkCapacity;
    this->inkAmount = inkCapacity; 
}

Pen::~Pen() {}

int Pen::getInkAmount() const {
    return inkAmount;
}

int Pen::getInkCapacity() const {
    return inkCapacity;
}

void Pen::write(Paper& paper, const std::string& message) {
    int symbolsAllowed = paper.getMaxSymbols() - paper.getSymbols();

    if ( inkAmount == 0 ) {
        throw OutOfInk();
    }

    if ( symbolsAllowed == 0 ) {
        throw OutOfSpace();
    }

    if ( message.size() > inkAmount ) {
        if ( inkAmount > symbolsAllowed ) {
            paper.addContent(message.substr(0, symbolsAllowed));
            inkAmount -= symbolsAllowed;
        } else {
            paper.addContent(message.substr(0, inkAmount));
            inkAmount = 0;
        }
    } else {
        if ( message.size() > symbolsAllowed ) {
            paper.addContent(message.substr(0, symbolsAllowed));
            inkAmount -= symbolsAllowed;
        } else {
            paper.addContent(message);
            inkAmount -= message.size();     
        }
    }    
}

void Pen::refill() {
    inkAmount = inkCapacity;
}