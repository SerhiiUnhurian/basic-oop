#include <iostream>
#include "Paper.h"

Paper::Paper(int maxSymbols) {
    symbols = 0;
    this->maxSymbols = maxSymbols;
}

Paper::~Paper() {}

int Paper::getMaxSymbols() const {
    return maxSymbols;
}

int Paper::getSymbols() const {
    return symbols;
}

void Paper::addContent(const std::string& message) {
    content.append(message);
    symbols += message.size();       
}

void Paper::show() const {
    std::cout << content << std::endl;
}