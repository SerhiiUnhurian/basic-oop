#include <iostream>
#include <cmath>
#include <string>
#include "Point.h"
#include "Car.h"

Car::Car(double capacity, double consumption, const Point& location, const std::string& model) { 
    fuelAmount = 0;
    fuelCapacity = capacity;
    fuelConsumption = consumption;
    this->location = location;
    this->model = model;
}

Car::~Car() {}

double Car::getFuelAmount() const {
    return fuelAmount;
}

double Car::getFuelCapacity() const {
    return fuelCapacity;
}

double Car::getFuelConsumption() const {
    return fuelConsumption;
}

const Point& Car::getLocation() const {
    return location;
}

const std::string& Car::getModel() const {
    return model;
}

void Car::drive(const Point& destination) {
    double distance = hypot(destination.getX() - location.getX(), destination.getY() - location.getY());
    double fuelNeeded = distance * fuelConsumption;
    if ( fuelNeeded > fuelAmount ) {
        throw OutOfFuel();
    }
    location = destination;
    fuelAmount -= fuelNeeded;
}

void Car::drive(double x, double y) {
    double distance = hypot(x - location.getX(), y - location.getY());
    double fuelNeeded = distance * fuelConsumption;
    if ( fuelNeeded > fuelAmount ) {
        throw OutOfFuel();
    }
    location = Point(x, y);
    fuelAmount -= fuelNeeded;
}

void Car::refill(double fuel) {
    double fuelAllowed = fuelCapacity - fuelAmount;
    if ( fuel > fuelAllowed ) {
        throw ToMuchFuel();
    }
    fuelAmount = fuel;
}


std::ostream& operator<<(std::ostream& out, const Car& car) {
    out << "Fuel amount = " << car.getFuelAmount() << std::endl;
    out << "Fuel capacity = " << car.getFuelCapacity() << std::endl;
    out << "Fuel consumption = " << car.getFuelConsumption() << std::endl;
    out << "Car location = " << car.getLocation() << std::endl;
    out << "Car model = " << car.getModel() << std::endl;

    return out;
}
