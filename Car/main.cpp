#include <iostream>
#include "Point.h"
#include "Car.h"
using namespace std;

int main() {
	Car merc;
	cout << merc << endl;

	try {
		merc.refill(60.0);

		Point p(2.5, 3.0);
		merc.drive(p);
		cout << merc << endl;
		merc.drive(10.7, 7.4);
		cout << merc << endl;
	} 
	catch(const OutOfFuel excep) {
		cout << "Not enough fuel." << endl;

	}
	catch(const ToMuchFuel excep) {
		cout << "To much fuel" << endl;
	}	

	return 0;
}