#include <iostream>
#include <cmath>
#include "Point.h"

Point::Point(double x, double y) {
    this->x = x;
    this->y = y;
}

Point::~Point() {}

double Point::getX() const {
    return x;
}
double Point::getY() const {
    return y;
}

void Point::setX(double value) {
    x = value;
}
void Point::setY(double value) {
    y = value;
}

double Point::distance(const Point& other) const {
    return hypot(x-other.x, y-other.y);
}
bool Point::operator==(const Point& other) const {
    return x == other.x && y == other.y;
}
bool Point::operator!=(const Point& other) const {
    return x != other.x && y != other.y;
}

std::ostream& operator<<(std::ostream& out, const Point& point) {
    return out << "(" << point.getX() << ", " << point.getY() << ")";
}