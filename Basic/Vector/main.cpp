#include <iostream>
#include "Vector.h"

int main() {
	Vector v1, v2(2.5, 1.7), v3;

	std::cout << v1 << std::endl;
	std::cout << v2 << std::endl;

	v1.setX(2.5);
	v1.setY(1.7);

	std::cout << v1.getX() << std::ends << v1.getY() << std::endl;
	std::cout << v1.len() << std::ends << v2.len() << std::endl;
	
	v3 = v1 + v2;
	std::cout << v3 << std::endl;

	v3 = v1 - v2;
	std::cout << v3 << std::endl;

	v3 -= v2;
	std::cout << v3 << std::endl;
	
	std::cout << (v3 == v1);
	std::cout << (v3 != v1);

	return 0;
}