#include <iostream>
#include <cmath>
#include "Vector.h"

Vector::Vector(double x, double y) {
	this->x = x;
	this->y = y;
}

Vector::~Vector() {}

double Vector::getX() const {
	return x;
}

double Vector::getY() const {
	return y;
}

void Vector::setX(double value) {
	x = value;
}

void Vector::setY(double value) {
	y = value;
}

double Vector::len() const {
	return hypot(x, y);
}

bool Vector::operator==(const Vector& other) const {
	return x == other.x && y == other.y;
}

bool Vector::operator!=(const Vector& other) const {
	return x != other.x && y != other.y;
}

void Vector::operator+=(const Vector& other) {
	x += other.x;
	y += other.y;
}

void Vector::operator-=(const Vector& other) {
	x -= other.x;
	y -= other.y;
}

Vector Vector::operator+(const Vector& other) const {
	Vector temp;
	temp.x = x + other.x;
	temp.y = y + other.y;
	return temp;
}

Vector Vector::operator-(const Vector& other) const {
	Vector temp;
	temp.x = x - other.x;
	temp.y = y - other.y;
	return temp;
}

std::ostream& operator<<(std::ostream& out, const Vector& vector) {
	return out << "(" << vector.getX() << ", " << vector.getY() << ")";
}