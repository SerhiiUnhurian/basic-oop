#include <iostream>
#include <cstdlib>
#include "List.h"

List::List(int capacity, double multiplier) {
    this->capacity = capacity;
    this->multiplier = multiplier;
    this->current = 0;
    this->array = (int*)malloc(capacity * sizeof(int));

    if ( this->array == NULL ) {
        throw OutOfMemoryException();
    }
}

List::~List() {
    std::cout << "destructing list" << std::endl;
    delete this->array;
}

int List::size() const {
    return this->current;
}

int List::max_size() const {
    return this->capacity;
}

void List::erase(int index) {
    if ( this->current == 0 ) {
        throw ZeroLenException();
    }
    if ( index < this->current && index >= 0 ) {
        for ( int i = index + 1; i < this->current; i++ ) {
            array[i-1] = array[i];
        }
        this->current -= 1;  
    }
        
}

void List::insert(int value, int index) {
	if ( index >= 0 && index < current ) {
	    if ( this->current == this->capacity ) {
	        int newCapacity = this->capacity * this->multiplier;
	        int *newArray = (int*)realloc(array, newCapacity * sizeof(int));

	        if ( newArray == NULL ) {
	            throw OutOfMemoryException();
	        }
            free(this->array);
	        this->array = newArray;
	        this->capacity = newCapacity;
	    }
	    for ( int i = this->current - 1; i >= index; i-- ) {
	        this->array[i+1] = this->array[i];
	    }
	    this->array[index] = value;
	    this->current += 1;
	}
}

int List::find(int value) const {
    for ( int i = 0; i < current; i++ ) {
        if ( array[i] == value ) {
            return i;
        }
    }
    return -1;
}

void List::push_back(int value) {
    if ( current == capacity ) {
        int newCapacity = capacity * multiplier;
        int *newArray = (int*)realloc(array, newCapacity * sizeof(int));

        if ( newArray == NULL ) {
            throw OutOfMemoryException();
        }
        free(this->array);
        this->array = newArray;
        this->capacity = newCapacity;
    }
    array[current] = value; // we could do: array[current++] = value;
    current += 1;
}

int List::pop_back() {
    if ( current <= 0 ) {
        throw ZeroLenException();
    }
    current -= 1;
    return array[current]; // we could write as well: return array[--current]; 
}

int partition(int array[], int start, int end) {
    int index = (start + end) / 2;
    int i = start;
    int temp = array[end];
    
    array[end] = array[index];
    array[index] = temp;
    
    for ( ; array[i] < array[end]; i++ );
    for ( int j = i + 1; j < end; j++ ) {
        if ( array[j] < array[end] ) {
            temp = array[j];
            array[j] = array[i];
            array[i] = temp;
            i += 1;
        }
    }
    
    temp = array[end];
    array[end] = array[i];
    array[i] = temp;
    
    return i;
}

void quickSort(int array[], int start, int end) {      
    if ( start < end ) {
        int pivot = partition(array, start, end);

        quickSort(array, start, pivot-1);
        quickSort(array, pivot+1, end);
    }
}

void List::sort() {
    quickSort(array, 0, current-1);
}

int List::operator[](int index) const {
    return array[index];
}

bool List::operator==(const List& other) const {
    if ( this->current != other.current ) {
        return false;
    }
    for ( int i = 0; i < this->current; i++ ) {
        if ( (*this)[i] != other[i] ) {
            return false;
        }
	}
    return true; 
}

// bool List::operator==(const List& other) const {
//     bool equal = 1;

//     if ( current != other.current ) {
//         equal = 0;
//         return equal;
//     }
//     for ( int i = 0; i < current; i++ ) {
//         if ( this->array[i] != other.array[i] ) { // не понятно почему 
//         	// вызывается деструктор при таком условии this[i] != other[i]
//             equal = 0;
//             break;
//         }
// 	}
//     return equal; 
// }
		    
bool List::operator!=(const List& other) const {
    return !(*this == other); 
}

std::ostream& operator<<(std::ostream& out, const List& list) {
    if ( list.size() == 0 ) {
        out << "List is empty." << std::endl;
    } else {
        int last = list.size() - 1;

        for ( int i = 0; i < last; i++ ) {
            out << list[i] << ' ';
        }
        out << list[last];
    }
    return out;
}
