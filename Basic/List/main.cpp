#include <iostream>
#include <cstdlib>
#include "List.h"

int main() {
    List l1(5, 1.2);
    List l2(5, 1.3);

    std::cout << "Size 1: "<< l1.size() << std::endl;
    std::cout << "Capacity: " << l1.max_size() << std::endl;
    // std::cout << "Size 2: "<< l2.size() << std::endl;
    l1.push_back(7);
    l1.push_back(1);
    l1.push_back(7);
    l1.push_back(5);
    l1.push_back(2);
    l1.push_back(2);
    

    std::cout << "Size 1: "<< l1.size() << std::endl;
    std::cout << "Capacity: " << l1.max_size() << std::endl;

    // l1.push_back(7);
    // l2.push_back(7);
    // l2.push_back(1);
    // l2.push_back(7);

    std::cout << (l1 == l2 ? "Equal" : "Not Equal") << std::endl;
	
 //    cout << "Size 2: "<< l2.size() << endl;

    // l1.push_back(5);
    // l1.push_back(2);
    // l1.push_back(2);


 //    // l1.insert(3, 0);
 //    // cout << "Size: "<< l1.size() << endl;
 //    cout << "List: " << l1 << endl;
 //    cout << "List: " << l2 << endl;
 //    // l1.erase(2);
 //    // cout << l1.size() << endl;
 //    // l1.sort();
 //    // cout << l1.find(9) << endl;
 //    // cout << "List: " << l1 << endl;
 //    // cout << "pop " << l1.pop_back() << endl;
    
 //    cout << "Capacity: " << l1.max_size() << endl;
 //    cout << "Capacity: " << l2.max_size() << endl;

    return 0;
}