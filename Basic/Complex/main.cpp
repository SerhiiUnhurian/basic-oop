#include <iostream>
#include "Complex.h"

int main() {
	Complex	x(2, 3);
	Complex y(1, 2);
	Complex z;

	std::cout << "x: " << x << std::endl;
	std::cout << "y: " << y << std::endl;
	std::cout << "z: " << z << std::endl;

	if ( x == y ) {
	std::cout << x << " is equal to " << y << std::endl;
	} else {
	std::cout << x << " is not equal to " << y << std::endl;
	}

	if ( x != y ) {
	std::cout << x << " != " << y << std::endl;
	} else {
	std::cout << x << " == " << y << std::endl;
	}

	z = x + y;
	std::cout << z.getReal() << "," << z.getImaginary() << std::endl;

	z -= x;
	std::cout << "z: " << z << std::endl;

	z -= x;
	std::cout << "z: " << z << std::endl;

	z = x * y;
	std::cout << "z: " << z << std::endl;

	return 0;
}