#include <iostream>
#include "Date.h"

Date::Date(int day, int month, int year) {
    // try {
        validate(day, month, year);
        this->day = day;
        this->month = month;
        this->year = year;
    // }
    // catch(const InvalidDate& error) {
    //     std::cout << error.text << std::endl;
    // }  
}

Date::~Date() {}

void Date::validate(int day, int month, int year) {
    int daysInMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30 , 31};

    if ( year % 400 == 0 ) {
        daysInMonth[1] += 1; 
    } else {
        if (year % 100 != 0 && year % 4 == 0 ) {
            daysInMonth[1] += 1;
        }
    }

    if ( month < 1 || month > 12) {
        throw InvalidDate("Invalid month.");
    } 

    if ( day < 1 || day > daysInMonth[month-1] ) {
        throw InvalidDate("Invalid day.");
    }
}

int Date::getDay() const {
    return day;
}
int Date::getMonth() const {
    return month;
}
int Date::getYear() const {
    return year;
}

std::ostream& operator<<(std::ostream& out, const Date& date) {
    out << date.getDay() << "." << date.getMonth() << "." << date.getYear() << std::endl;
    return out;
}
