#include <iostream>
#include "Date.h"

int main() {
    int dd, mm, yy; 
    std::cin >> dd >> mm >> yy;
    try {
        Date date(dd, mm, yy); 
        std::cout << date << std::endl;
    }
    catch(const InvalidDate& error) {
        std::cout << error.text << std::endl;
    }
    
    return 0;
}